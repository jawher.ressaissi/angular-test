from flask import Flask, request, jsonify
import requests
import ast
import pandas as pd
from flask_cors import CORS
import json

app = Flask(__name__)
CORS(app, resources={"*": {"origins": "*"}})

@app.route('/api/2', methods=['GET'])
def get_request():
    # Get the parameters from the query string
    uai = request.args.get('uai')
    year = request.args.get('year')
    url = f"https://data.education.gouv.fr/api/records/1.0/search/?dataset=fr-en-dnma-par-uai-appareils&q=&rows=100&facet=debutsemaine&facet=uai&refine.debutsemaine={year}&refine.uai={uai}"
    r = requests.get(url)
    result = r.content
    data = ast.literal_eval(result.decode('utf8'))
    l = []
    for i in range(len(data["records"])):
        l.append(data["records"][i]["fields"])
    df = pd.DataFrame(l)
    df = df [["visites_macos", "visites_windows", "visites_android", "debutsemaine"]]
    df.debutsemaine = df.debutsemaine.apply(lambda x : int(x[:7].split("-")[-1]))
    df = df.groupby(["debutsemaine"]).sum()
    df.reset_index(inplace=True)
    s = df.to_json()
    json_obj = json.loads(s)
    return jsonify({"result": json_obj})

if __name__ == '__main__':
    app.run(debug=True)
