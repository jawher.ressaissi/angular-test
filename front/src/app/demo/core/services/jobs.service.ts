import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};



@Injectable({
  providedIn: 'root'
})
export class JobsService {


  constructor(private http: HttpClient) { }
private apiURL= "http://localhost:9075/jobs";

getWordCloud (): Observable<any> {
  return this.http.get(this.apiURL+'/wordcloud', httpOptions);
}
getLocation (): Observable<any> {
  return this.http.get(this.apiURL+'/location' , httpOptions);
}
getSource (): Observable<any> {
  return this.http.get(this.apiURL+'/source' , httpOptions);
}
geTitle (): Observable<any> {
  return this.http.get(this.apiURL+'/title' , httpOptions);
}

}
