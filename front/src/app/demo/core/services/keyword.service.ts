import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
export class KeywordService {
  api_key = 'b08f131bc1bb413f983feb422f347d58';
  

  constructor(private http: HttpClient) { }

private api= "http://localhost:9200/";
private apiKeyyout= "http://localhost:9009/";

trackKey(keyword : string , since : any): Observable<any> {
  return this.http.get(this.api+'findkeyword/'+keyword+'/'+since,httpOptions);
}

helloKey(): Observable<any> {
  return this.http.get(this.api,httpOptions);
}
progress(keyword : string): Observable<any> {
  return this.http.get(this.apiKeyyout+'linechart/'+keyword);
}
youtubekey(keyword : string): Observable<any> {
  return this.http.get(this.apiKeyyout+'youtube/'+keyword);
}
youtubelyrics(): Observable<any> {
  return this.http.get(this.apiKeyyout+'youtubelyrics',httpOptions);
}
piedomains(): Observable<any> {
  return this.http.get(this.apiKeyyout+'piedomains',httpOptions);
}
Twitter(keyword : string , since : any): Observable<any> {
  return this.http.get(this.apiKeyyout+'twitter/'+keyword+'/'+since,httpOptions);
}
 
top(keyword : string): Observable<any> {
  return this.http.get(this.apiKeyyout+'topstories/'+keyword,httpOptions);
}
 
articles(): Observable<any> {
  return this.http.get(this.apiKeyyout+'articles',httpOptions);
}
events(keyword : string): Observable<any> {
  return this.http.get(this.apiKeyyout+'events/'+keyword,httpOptions);
}
}
