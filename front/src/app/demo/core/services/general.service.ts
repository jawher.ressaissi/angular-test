import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};



@Injectable({
  providedIn: 'root'
})
export class GeneralService {


  constructor(private http: HttpClient) { }
private apiURL= "http://localhost:9075/";
private api= "http://localhost:9100/";


getSubdomains (company : any): Observable<any> {
  return this.http.get(this.apiURL+'/subdomains/' + company, httpOptions);
}
getInfluencial (company : any): Observable<any> {
  return this.http.get(this.apiURL+'/influencial/' + company, httpOptions);
}
getSocial (company : any): Observable<any> {
  return this.http.get(this.apiURL+'/followers/' + company, httpOptions);
}
getBacklinks(company : any): Observable<any> {
  return this.http.get(this.apiURL+'/Backlinks/' + company, httpOptions);



}
getEvents(company : any): Observable<any> {
  return this.http.get(this.apiURL+'events/' + company, httpOptions);
}
getTechnologies(): Observable<any> {
  return this.http.get(this.apiURL+'technology/' , httpOptions);
}
getHashtags(): Observable<any> {
  return this.http.get(this.apiURL+'hashtags/' , httpOptions);
}
getERF(): Observable<any> {
  return this.http.get(this.apiURL+'ERF/' , httpOptions);
}
getERI(): Observable<any> {
  return this.http.get(this.apiURL+'ERI/' , httpOptions);
}
getERL(): Observable<any> {
  return this.http.get(this.apiURL+'ERL/' , httpOptions);
}
get_site_audit(): Observable<any> {
  return this.http.get(this.apiURL+'site_audit/' , httpOptions);
}
get_site_download(): Observable<any> {
  return this.http.get(this.apiURL+'download_latency/' , httpOptions);
}
trackKey(): Observable<any> {
  return this.http.get(this.api+'findkeyword/', httpOptions);
}

getArticles(keyword : any): Observable<any> {
  return this.http.get(this.api+'medium/'+ keyword, httpOptions);}
  
  getImage(imageUrl: string): Observable<Blob> {
    return this.http.get(imageUrl, { responseType: 'blob' });
  }
}  
