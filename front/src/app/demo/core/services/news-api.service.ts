import { Injectable } from '@angular/core';
import { HttpClient  } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class NewsApiService {
  
  api_key = 'b08f131bc1bb413f983feb422f347d58';
  

  constructor(private http:HttpClient) { }




  initArticles(keyword:string,since:string){
    const now = new Date()
    const tos=now.toISOString().split('T')[0];
   return this.http.get('https://newsapi.org/v2/everything?q='+keyword+'&from='+since+'&to='+tos+'&sortBy=popularity&apiKey='+this.api_key);
  }




}
