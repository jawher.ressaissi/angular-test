import { Component, OnInit } from "@angular/core";
import { GeneralService } from "../../core/services/general.service";
import { JobsService } from "../../core/services/jobs.service";
import { CalendarOptions } from '@fullcalendar/angular';
import { ChartDB } from "../../../fack-db/chart-data";
import { ApexChartService } from "../../../theme/shared/components/chart/apex-chart/apex-chart.service";
import { CallBackendService } from 'src/app/demo/services/call-backend.service';
import { ApiConfig } from 'src/app/demo/services/api-path';
import { HttpClient } from '@angular/common/http';
// wordcloud

@Component({
  selector: "app-dash-analytics",
  templateUrl: "./dash-analytics.component.html",
  styleUrls: ["./dash-analytics.component.scss"],
})
export class DashAnalyticsComponent implements OnInit {
  Events: [];
  calendarOptions: CalendarOptions;
  public chartDB: any;
  public dailyVisitorStatus: string;
  public dailyVisitorAxis: any;
  public deviceProgressBar: any;
  public isCollapsed: boolean;
  public submitLoader: boolean;
  public submitLoaderDown: boolean;
  formBuilder: any;
  sites: string | "";
  company: string; // the company to scrap
  medias: string;// list of media in the checkbox
  subs: any;// subdomains
  social: any
  socials : [123,5698,3215]// social media followers
  public infs: any;
  public infp: any; //influencial sites: variable length
  infs_sites: any;// influencial sites: liste of sites
  // variables for the Section Job Part:
  titles: any;
  value: any
  key: any;
  list_val = [];
  list_key = [];
   // variables for the Section Job Part:
  sources: any;
  value_source: any
  key_source: any;
  list_valS = [];
  list_keyS = [];
    // variables for the Section Job Part:
    location: any;
    value_loc: any
    key_loc: any;
    list_valL = [];
    list_keyL = [];
    backlink : any;
    // variables for the Section Job Part:
    tech: any;
    value_tech: any
    key_tech: any;
    list_valT = [];
    list_keyT = [];
    public ert: any
    eri: any
    public erl : any
    public wordcloud : any

    // variables for the Section Job Part:
    hash: any;
    value_hash: any
    key_hash: any;
    list_valH = [];
    list_keyH = [];
    site_aud: any;
    download_aud: any;
    isShow= false;
    api_config:ApiConfig = new ApiConfig();



  constructor(public apexEvent: ApexChartService, private generalservice : GeneralService, private jobsservice: JobsService , private httpClient: HttpClient, private callBackendService: CallBackendService) {
    this.chartDB = ChartDB;
    this.dailyVisitorStatus = "1y";

    this.deviceProgressBar = [
      {
        type: "success",
        value: 66,
      },
      {
        type: "primary",
        value: 26,
      },
      {
        type: "danger",
        value: 8,
      },
    ];
    this.submitLoader = false;

    // wordcloud
  }

  dailyVisitorEvent(status) {
    this.dailyVisitorStatus = status;
    switch (status) {
      case "1m":
        this.dailyVisitorAxis = {
          min: new Date("28 Jan 2013").getTime(),
          max: new Date("27 Feb 2013").getTime(),
        };
        break;
      case "6m":
        this.dailyVisitorAxis = {
          min: new Date("27 Sep 2012").getTime(),
          max: new Date("27 Feb 2013").getTime(),
        };
        break;
      case "1y":
        this.dailyVisitorAxis = {
          min: new Date("27 Feb 2012").getTime(),
          max: new Date("27 Feb 2013").getTime(),
        };
        break;
      case "ytd":
        this.dailyVisitorAxis = {
          min: new Date("01 Jan 2013").getTime(),
          max: new Date("27 Feb 2013").getTime(),
        };
        break;
      case "all":
        this.dailyVisitorAxis = {
          min: undefined,
          max: undefined,
        };
        break;
    }

    setTimeout(() => {
      this.apexEvent.eventChangeTimeRange();
    });
  }
  onDateClick(res) {
    alert('Clicked on date : ' + res.dateStr)
  }

  ngOnInit() {
    this.isCollapsed = true;
    this.listtitles();
    this.listsources()
    this.listLocation()
    this.listEvents()
    this.listTechnologies()

    this.listHashtags()
    this.siteDownload()
    this.siteAudit()

    setTimeout(() => {
      return this.listEvents()
    }, 2200);

    setTimeout(() => {
      this.calendarOptions = {
        initialView: 'dayGridMonth',
        dateClick: this.onDateClick.bind(this),
        events: this.Events
      };
    }, 2500);
    console.log("ahla"+ this.Events)
    this.listInfluencial()

    }

  checkCheckBoxvalue(e: any) {
    let d: string = e.target.id;
    this.sites = this.sites + "-" + d;
  }

  onScrap() {
    this.submitLoader = true;

    let d = this.sites.toString();
    this.medias = d.slice(10);

    setTimeout(() => {
      this.submitLoader = false;
    }, 3000);
    this.listSubdomains();
    this.listInfluencial();
    this.listSocials();
    this.listbacklinks()
    this.engagement_rate_L()
    this.engagement_rate_T()
    this.engagement_rate_I()
    this.isShow = true;



    return {
      company: this.company ,
      medias: this.medias,



  };}
  onDownload(){
    this.submitLoaderDown = true;
    setTimeout(() => {
      this.submitLoaderDown = false;
    }, 3000);
  }
  listSubdomains() : void {
    this.generalservice.getSubdomains(this.company)
      .subscribe(
        res => {
          this.subs = res;
        },
        error => {
          console.log(error);
        });
  }
  listSocials() : void {
    this.generalservice.getSocial(this.company)
      .subscribe(
        res => {
          this.social = Array.of(res);
          console.log(res)
        },
        error => {
          console.log(error);
        });
  }

  listInfluencial() : void {
    this.callBackendService.getRessource(this.api_config.stat_medium).subscribe(resm => {
      
      this.infs = resm
      if(this.infs!=null){
        this.infs = this.infs
      }
        },
        
        error => {
          console.log(error);
        });

        this.callBackendService.getRessource(this.api_config.stat_ieee).subscribe(resi => {
      
          this.infp = resi
          if(this.infp!=null){
            this.infp = this.infp
          }},
          error => {
            console.log(error);
          });

          this.callBackendService.getRessource(this.api_config.stat_youtube).subscribe(resy => {
      
            this.ert = resy
            if(this.ert!=null){
              this.ert = this.ert.total_videos
            }},
            error => {
              console.log(error);
            });

            this.callBackendService.getRessource(this.api_config.stat_twitter).subscribe(rest => {
      
              this.erl = rest
              if(this.erl!=null){
                this.erl = this.erl.total_tweets
              }},
              error => {
                console.log(error);
              });

              this.wordcloud = "http://localhost:5000/static/wordcloud.png"
  }



  listtitles() : void {
    this.jobsservice.geTitle()
      .subscribe(
        res => {
          this.titles =res;

          for ( this.key in this.titles) {
            this.value = this.titles[this.key];
            this.list_val.push(this.value);
            this.list_key.push(this.key);
            this.chartDB.barJobs.xaxis.categories.push(this.key)
            this.chartDB.barJobs.series[0].data.push(this.value)

      ;}
        },
        error => {
          console.log(error);
        });
  }

  listsources() : void {
    this.jobsservice.getSource()
      .subscribe(
        res => {
          this.sources =res;
          for ( this.key_source in this.sources) {
            this.value_source = this.sources[this.key_source];
            this.list_valS.push(this.value_source);
            this.list_keyS.push(this.key_source);

             this.chartDB.pieSource.labels.push(this.key_source)
             this.chartDB.pieSource.series.push(this.value_source)

      ;}

        },
        error => {
          console.log(error);
        });
  }

  listLocation() : void {
    this.jobsservice.getLocation()
      .subscribe(
        res => {
          this.location =res;
          for ( this.key_loc in this.location) {
            this.value_loc = this.location[this.key_loc];
            this.list_valL.push(this.value_loc);
            this.list_keyL.push(this.key_loc);

             this.chartDB.pieLocation.labels.push(this.key_loc)
             this.chartDB.pieLocation.series.push(this.value_loc)

      ;}

        },
        error => {
          console.log(error);
        });
  }

  listbacklinks() : void {
    this.generalservice.getBacklinks(this.company)
      .subscribe(
        res => {
          this.backlink = Array.of(res);
          console.log("hello"+ res)
        },
        error => {
          console.log(error);
        });
  }

  listEvents() : void {
    this.generalservice.getEvents("talan")
    .subscribe(res => {
      this.Events = (res);
      console.log(this.Events); },
        error => {
          console.log(error);
        });
  }

  listTechnologies() : void {
    this.generalservice.getTechnologies()
    .subscribe(res => {
      this.tech = (res);
      console.log(this.tech);
      for ( this.key_tech in this.tech) {
        this.value_tech = this.tech[this.key_tech];
        this.list_valT.push(this.value_tech);
        this.list_keyT.push(this.key_tech);

         this.chartDB.pieTech.labels.push(this.key_tech)
         this.chartDB.pieTech.series.push(this.value_tech);}



    },
        error => {
          console.log(error);
        });
  }

  engagement_rate_T() : void {
    this.generalservice.getERF()
    .subscribe(res => {
      this.ert = (res);
      console.log(this.ert); },
        error => {
          console.log(error);
        });
  }
  engagement_rate_I() : void {
    this.generalservice.getERI()
    .subscribe(res => {
      this.eri = (res);
      console.log(this.eri); },
        error => {
          console.log(error);
        });
  }
  engagement_rate_L() : void {
    this.generalservice.getERL()
    .subscribe(res => {
      this.erl = (res);
      console.log(this.erl); },
        error => {
          console.log(error);
        });
  }
  listHashtags() : void {
    this.generalservice.getHashtags()
      .subscribe(
        res => {
          this.hash =res;
          console.log(this.hash)

          for ( this.key_hash in this.hash) {
            this.value_hash = this.hash[this.key_hash];
            this.list_valH.push(this.value_hash);
            this.list_keyH.push(this.key_hash);
            this.chartDB.pieHash.labels.push(this.key_hash)
            this.chartDB.pieHash.series.push(this.value_hash)

      ;}
        },
        error => {
          console.log(error);
        });
  }
  siteAudit() : void {
    this.generalservice.get_site_audit()
    .subscribe(res => {
      this.site_aud = (res);
      console.log(this.site_aud);
      this.chartDB.radialBar1CAC.series.push(this.site_aud)

    },
        error => {
          console.log(error);
        });
  }
  siteDownload() : void {
    this.generalservice.get_site_download()
    .subscribe(res => {
      this.download_aud = (res);
      console.log("hellooooooooo"+this.download_aud); },
        error => {
          console.log(error);
        });
  }


}
