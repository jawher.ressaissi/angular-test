import { APP_INITIALIZER, Component, OnInit } from '@angular/core';
import { ChartDB } from "../../../../fack-db/chart-data";
import { ApexChartService } from 'src/app/theme/shared/components/chart/apex-chart/apex-chart.service';
import { CallBackendService } from 'src/app/demo/services/call-backend.service';
import { ApiConfig } from 'src/app/demo/services/api-path';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-basic-elements',
  templateUrl: './basic-elements.component.html',
  styles: ['td {width:200px important;}', '.scrollit {overflow:scroll !important;height:500px !important;}'],
  styleUrls: ['./basic-elements.component.scss'],

})
export class BasicElementsComponent implements OnInit {
  //wordcloud dependencies start here
  public notifier: NotifierService;
  public chartDB: any;
  public year: any;
  public processing: boolean = false;
  public disp: boolean = false;
  public table_data: Array<any>;
  public uai: any;
  public show_table = false;
  public submitLoader: boolean = false;
  public data: any;


  api_config: ApiConfig = new ApiConfig();


  constructor(public apexEvent: ApexChartService, private callBackendService: CallBackendService, notifierService: NotifierService) {
    this.notifier = notifierService;
    this.submitLoader = false;
    this.chartDB = ChartDB;
  }

  ngOnInit() {

  }

  onSubmitLoader() {

    this.disp = false;
    this.submitLoader = true;
    this.show_table = false;

    this.notifier.notify('default', 'Request started');
    this.callBackendService.getRessource1(this.uai).subscribe(res => {
      this.submitLoader = false;
      this.processing = true;
      if (res != null) {
        this.table_data = res.records;
        this.show_table = true;
        this.callBackendService.getRessource2(this.uai, this.year).subscribe(res2 => {
          this.data = res2.result
          if (this.data != null) {
            this.dataForStat()
            this.processing = false;
            this.disp = true;
            this.notifier.notify('info', 'Request ended');
          }

        })
      }
    })

  }



  dataForStat() {
    this.chartDB.area1CAC.series[0].name = "Visites Windows"
    this.chartDB.area1CAC.series[1].name = "Visites Android"
    this.chartDB.area1CAC.series[2].name = "Visites MacOS"

    var arr1 = [];
    var arr2 = [];
    var arr3 = [];
    var arr4 = [];

    for (let i = 0; i < 12; i++) {

      arr1.push(this.data.debutsemaine[i]);
      arr2.push(parseInt(this.data.visites_windows[i]));
      arr3.push(parseInt(this.data.visites_android[i]));
      arr4.push(parseInt(this.data.visites_macos[i]));


    }
    this.chartDB.area1CAC.series[0].data = arr2;
    this.chartDB.area1CAC.series[1].data = arr3;
    this.chartDB.area1CAC.series[2].data = arr4;
    this.chartDB.area1CAC.xaxis.categories = arr1;

  }











}


