import { BasicElementsComponent } from './basic-elements.component';
import { NotifierService } from 'angular-notifier';
import { CallBackendService } from 'src/app/demo/services/call-backend.service';
import { of } from 'rxjs';

describe('BasicElementsComponent', () => {
  let component: BasicElementsComponent;
  let notifierService: NotifierService;
  let callBackendService: CallBackendService;

  beforeEach(() => {
    notifierService = jasmine.createSpyObj('NotifierService', ['notify']);
    callBackendService = jasmine.createSpyObj('CallBackendService', ['getRessource1', 'getRessource2']);
    component = new BasicElementsComponent(null, callBackendService, notifierService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onSubmitLoader', () => {
    it('should set submitLoader to true and other flags to false', () => {
      component.disp = true;
      component.submitLoader = false;
      component.show_table = true;
      component.onSubmitLoader();

      expect(component.disp).toBe(false);
      expect(component.submitLoader).toBe(true);
      expect(component.show_table).toBe(false);
    });

    it('should call the backend services correctly', () => {
      const mockResponse1 = { records: [{ key: 'value' }] };
      const mockResponse2 = { result: { debutsemaine: [], visites_windows: [], visites_android: [], visites_macos: [] } };
      
      (callBackendService.getRessource1 as jasmine.Spy).and.returnValue(of(mockResponse1));
      (callBackendService.getRessource2 as jasmine.Spy).and.returnValue(of(mockResponse2));

      component.uai = '123456789';
      component.year = '2023';
      component.onSubmitLoader();

      expect(callBackendService.getRessource1).toHaveBeenCalledWith('123456789');
      expect(callBackendService.getRessource2).toHaveBeenCalledWith('123456789', '2023');
      expect(component.table_data).toEqual(mockResponse1.records);
      expect(component.show_table).toBe(true);
      expect(component.data).toEqual(mockResponse2.result);
      expect(component.disp).toBe(true);
    });
  });

  describe('dataForStat', () => {
    it('should set the chartDB properties correctly', () => {
      component.data = {
        debutsemaine: ['Jan', 'Feb', 'Mar'],
        visites_windows: ['100', '200', '300'],
        visites_android: ['50', '150', '250'],
        visites_macos: ['300', '250', '200'],
      };

      component.dataForStat();

      expect(component.chartDB.area1CAC.series[0].data).toEqual([100, 200, 300]);
      expect(component.chartDB.area1CAC.series[1].data).toEqual([50, 150, 250]);
      expect(component.chartDB.area1CAC.series[2].data).toEqual([300, 250, 200]);
      expect(component.chartDB.area1CAC.xaxis.categories).toEqual(['Jan', 'Feb', 'Mar']);
    });
  });
});
