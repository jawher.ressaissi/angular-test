import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighchartsChartModule } from 'highcharts-angular';
import { BasicElementsRoutingModule } from './basic-elements-routing.module';
import { BasicElementsComponent } from './basic-elements.component';
import {SharedModule} from '../../../../theme/shared/shared.module';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxHorizontalTimelineModule } from 'ngx-horizontal-timeline';
import { NgxTweetModule } from "ngx-tweet";
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import { NotifierModule } from 'angular-notifier';
FullCalendarModule.registerPlugins([
  dayGridPlugin,
  interactionPlugin
]);
@NgModule({
  imports: [
    CommonModule,
    NgxTweetModule,
    HighchartsChartModule,
    BasicElementsRoutingModule,
    SharedModule,
    FullCalendarModule,
    NgbDropdownModule,
    NgxHorizontalTimelineModule,
    NotifierModule
  ],
  declarations: [BasicElementsComponent]
})
export class BasicElementsModule { }
