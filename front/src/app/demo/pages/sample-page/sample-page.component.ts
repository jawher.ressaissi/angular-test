import { Component, OnInit } from '@angular/core';
import { TimelineItem } from 'ngx-horizontal-timeline';
@Component({
  selector: 'app-sample-page',
  templateUrl: './sample-page.component.html',
  styleUrls: ['./sample-page.component.scss']
})
export class SamplePageComponent implements OnInit {

  constructor() { }
  items: TimelineItem[] = [];
  ngOnInit() {
    this.items.push({
      label: '25-4-2022',
      icon: 'far fa-circle',


      color: '004a9e',
      command() {
        console.log('Action 0');
      }
    });

    this.items.push({
      label: 'Test 2',
      icon: 'fa fa-address-book-o',

      color: '004a9e',
      command() {
        console.log('Action 2');
      }
    });
    this.items.push({
      label: 'Test 2',
      icon: 'fa fa-address-book-o',

      color: '004a9e',
      command() {
        console.log('Action 2');
      }
    });
    this.items.push({
      label: 'Test 2',
      icon: 'fa fa-address-book-o',

      color: '004a9e',
      command() {
        console.log('Action 2');
      }
    });
    this.items.push({
      label: 'Test 2',
      icon: 'fa fa-address-book-o',

      color: '004a9e',
      command() {
        console.log('Action 2');
      }
    });
    this.items.push({
      label: 'Test 2',
      icon: 'fa fa-address-book-o',

      color: '004a9e',
      command() {
        console.log('Action 2');
      }
    });
    this.items.push({
      label: 'Test 2',
      icon: 'fa fa-address-book-o',

      color: '004a9e',
      command() {
        console.log('Action 2');
      }
    });
    this.items.push({
      label: 'Test 2',
      icon: 'fa fa-address-book-o',

      color: '004a9e',
      command() {
        console.log('Action 2');
      }
    });
    this.items.push({
      label: 'Test 2',
      icon: 'fa fa-address-book-o',

      color: '004a9e',
      command() {
        console.log('Action 2');
      }
    });
    this.items.push({
      label: 'Test 2',
      icon: 'fa fa-address-book-o',

      color: '004a9e',
      command() {
        console.log('Action 2');
      }
    });
  }

}
