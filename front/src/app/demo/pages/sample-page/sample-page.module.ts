import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxHorizontalTimelineModule } from 'ngx-horizontal-timeline';
import { SamplePageRoutingModule } from './sample-page-routing.module';
import { SamplePageComponent } from './sample-page.component';
import {SharedModule} from '../../../theme/shared/shared.module';

@NgModule({
  declarations: [SamplePageComponent],
  imports: [
    CommonModule,
    SamplePageRoutingModule,
    SharedModule,
    NgxHorizontalTimelineModule
  ]
})
export class SamplePageModule { }
