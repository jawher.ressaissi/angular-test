import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiConfig } from './api-path';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CallBackendService {
  private apiUrl1 = 'https://data.education.gouv.fr/api/records/1.0/search/?dataset=fr-en-dnma-par-uai-appareils&q=&rows=10&facet=debutsemaine&facet=uai';
  private apiUrl2 = 'http://localhost:5000/api/2'
  private api_config:ApiConfig = new ApiConfig();
  
  constructor(private http:HttpClient) { }

  /**
   * getRessourcePublic
   */
  public getRessource1(uai: string) : Observable<any> {
    const url = `${this.apiUrl1}?refine.uai=${uai}`;

    // Make the GET request and return the observable
    return this.http.get<any>(url);
  }

  public getRessource(uai: string) : Observable<any> {
    const url = `${this.apiUrl1}?refine.uai=${uai}`;

    // Make the GET request and return the observable
    return this.http.get<any>(url);
  }

  public getRessource2(uai: string, year: string) : Observable<any> {
    const url = `${this.apiUrl2}?uai=${uai}&year=${year}`;

    // Make the GET request and return the observable
    return this.http.get<any>(url);
  }
  
  /**
   * postRessourcePublic
   */
   public postRessource(api_path:any, ressource:any) {
    return this.http.post(this.api_config.host+api_path,ressource);
  }
}
